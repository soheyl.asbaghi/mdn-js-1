console.log('hello world');

// a one line comment

/* this is a longer,
 * multi-line comment
 */

// TODO: (Variables)
// The 'var' keyword is used in pre-ES6 versions of JS.
var myName = 'James';
// 'let' is the preferred way to declare a variable when it can be reassigned.
let myFamily = 'Bond';
// 'const' is the preferred way to declare a variable with a constant value.
const fullName = `I'm ${myFamily}, ${myName} ${myFamily}.`;
console.log(fullName);
document.write(fullName);

/* _Variable scope */

const city = 'New York City';
function logCitySkyline() {
  let skyscraper = 'Empire State Building';
  return `The stars over the ${skyscraper} in ${city}`;
}
console.log(logCitySkyline());

const satellite = 'The Moon';
const galaxy = 'The Milky Way';
const stars = 'North Star';

function callMyNightSky() {
  return 'Night Sky: ' + satellite + ', ' + stars + ', and ' + galaxy;
}
console.log(callMyNightSky());

function logVisibleLightWaves() {
  const lightWaves = 'Moonlight';
  console.log(lightWaves);
}
console.log(logVisibleLightWaves());
// console.log(lightWaves); //!Error

/* _Data type */

const unknown1 = 'foo';
console.log(typeof unknown1);

const unknown2 = 10;
console.log(typeof unknown2);

const unknown3 = true;
console.log(typeof unknown3);

const unknown4 = {
  made: 'Ford',
  model: 'Mustang',
  year: 1969,
};
console.log(typeof unknown4);

// _Convert
let num1 = '6' - 10;
console.log(num1); // -4
let num2 = '6' + 10;
console.log(num2); // 610

console.log(parseFloat('4.22')); //4.22
console.log(parseFloat('hello 222.4')); //NaN
console.log(parseInt('101', 2)); //4
console.log(parseInt('hello 222.4')); //NaN

/* _Array */
const dcHero = ['Superman', 'Batman', 'Robin'];
console.log(dcHero);
console.log(dcHero.length);

const mrHero = ['IronMan', , 'SpiderMan', 'CaptainAmerica'];
console.log(mrHero);
console.log(mrHero[1]);
console.log(mrHero.length);

/* _Object */
const car = {
  //key: 'Value'
  make: 'Ford',
  model: 'Mustang', //Property
  year: 1969,
  price: '74.5 $',
  engine: {
    history: 'Rebuilt',
    condition: 'Running',
  },
  speed() {
    return '220 Km/h'; //Method
  },
};
console.log(car.make);
console.log(car.engine.history);
console.log(car.speed());

// TODO: (Control flow and error handling)

/* _Conditional statements */

//if and else statement
if (parseFloat(car.price) <= 80 /*condition-1*/) {
  console.log('I can buy this car'); /*statements-1*/
} else if (parseFloat(car.price) >= 80 /*condition-2*/) {
  console.log('I can buy this car next month'); /*statements-2*/
} else {
  console.log(`I can't buy this car`); /*statements-3*/
}

//switch statement
// switch (fruitType) {
//   case 'Oranges':
//     console.log('Oranges are $0.59 a pound.'); /*statements-1*/
//     break;
//   case 'Apples':
//     console.log('Apples are $0.32 a pound.'); /*statements-2*/
//     break;
//   case 'Bananas':
//     console.log('Bananas are $0.48 a pound.'); /*statements-3*/
//     break;
//   default:
//     console.log(`Sorry, we are out of ${fruitType}.`); /*statements-def*/
// }
// console.log("Is there anything else you'd like?");

//throw, try, catch, finally statement;
/*
throw 404;
throw 'Error 404';
*/

try {
  // try statement
  console.log('Start of try runs');
  unicycle;
  console.log('end of try runs -- never reached');
} catch (err) {
  // catch statement
  console.log('Error has occurred: ' + err.stack);
} finally {
  // finally statement
  console.log('This is always run');
}

console.log('...Then the execution continues');
